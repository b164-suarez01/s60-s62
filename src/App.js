import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavBar';
import ProductView from './pages/ProductView';
import Home from './pages/Home';
import Products from './pages/Products'
import Register from './pages/Register';
import Login from './pages/Login';
import AdminPage from './pages/AdminPage';
import Logout from './pages/Logout';
import ErrorPage from './pages/Error';
import FooterPage from './components/Footer';
import OrderHistoryView from './pages/OrderHistory';
import AllProducts from './pages/allProduct';
import './App.css';
import {UserProvider} from './UserContext';

function App() {

//React Context is nothing but a global state to the app. It is a way to make a particular data available to all components no matter how thet are nested.

//State hook for the user state that defines here for a global scope
//Initialized as an obj. with prop from the local Storage
//This will be used to store the user info and will be used for validating if a user is logged in on the app or not
  const [user,setUser] = useState({
    id: null,
    isAdmin: null
  })
  //Function for clearing the localStorage on logout

  const unsetUser = () => {
    localStorage.clear()
  }
//useEffect nag tatake ng effect at least once pag nagload ung component. may function at optional dependency array as parameters. Pag may value ung dpendency array once lang magrurun ung component
  useEffect( () => {

    fetch("https://intense-castle-41238.herokuapp.com/api/users/details" , {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined") {
        setUser ({
          _id:data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })

  }, [])

  return (
    <UserProvider value={{user,setUser,unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path ="/" element ={<Home />} />
            <Route path ="/products" element ={<Products />} />
            <Route path ="/products/:productId" element ={<ProductView />} />
            <Route path ="/login" element ={<Login />} />
            <Route path ="/adminPage" element ={<AdminPage />} />
            <Route path ="/register" element ={<Register />} />
            <Route path ="/logout" element ={<Logout />} />
            <Route path ="/myOrder" element ={<OrderHistoryView />} />
           <Route path ="/allproducts" element ={<AllProducts />} />
          {/*  <Route path ="/myOrder" element ={<getOrders />} /> */}
            <Route path='*' element={<ErrorPage/>}   />
          </Routes>
        </Container>  
      </Router>
       <FooterPage />
    </UserProvider>

  );
}

export default App;

