import {useState, useEffect} from 'react';
import {Card} from'react-bootstrap';
import {Link} from 'react-router-dom'

export default function CartItemCard({cartProp}){
	//Use the state hook for this component to be able to store its state
	/*
		Syntax:
			const [getter,setter] = useState(initialGetterValue)
	
	*/

	const {quantity,productId,price}=cartProp;
	return (
			<Card className="cartItemCard">
				<Card.Body>
					<Card.Title>{quantity}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{productId}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
			
				
				
			</Card>

		)
}