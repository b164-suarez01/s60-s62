
import {Fragment, useContext} from 'react';
import {Navbar,Nav,Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar (){

	const {user} = useContext(UserContext);

/*
	Syntax:
		localStorage.getItem(propertyName)
*/

//const [user, setUser] = useState(localStorage.getItem("email"));


	return(
		<Navbar bg="light" expand="lg">
		  <Container>
		  	
		    <Navbar.Brand as={Link} to="/" className="logo">TLS</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  
		    <Navbar.Collapse id="basic-navbar-nav" >
		    
		    
		      <Nav className = "ms-auto">
		      	
		        <Nav.Link as={Link} to="/">Home</Nav.Link>
		        <Nav.Link as={Link} to="/products">Products</Nav.Link>
		      	
		      	{  (user.id !== null && user.isAdmin===false) ?
		      		<Fragment>
		      		<Nav.Link as={Link} to="/myOrder"> My Orders </Nav.Link>
		      		<Nav.Link as={Link} to="/logout"> Logout </Nav.Link>
		      		
		      		</Fragment>
		      		:
		      		(user.id !== null && user.isAdmin===true) ?
		      		<Fragment>
		      		<Nav.Link as={Link} to="/adminPage"> Admin Dashboard </Nav.Link>
		      		<Nav.Link as={Link} to="/logout"> Logout </Nav.Link>
		      		</Fragment>
		      		:
		      		<Fragment>
		      			<Nav.Link as={Link} to="/register">Register</Nav.Link>
		      			<Nav.Link as={Link} to="/login">Login</Nav.Link>
		      		</Fragment>
		      	
		      	}
		      

		      </Nav>
		    </Navbar.Collapse>
		 
		  </Container>
		</Navbar>

		)
}