import { useState } from 'react';

import { Row, Col, Card, Button,Modal,Form } from 'react-bootstrap';
import {Fragment} from 'react';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AdminPanel(props) {



	
	const [productId, setProductId] = useState("")

	//const { productId2 } = useParams()
;
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)


	const [showAdd, setShowAdd] = useState(false)
	
	const token = localStorage.getItem("token")


	//Functions to handle opening and closing modals
	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)


		const addProduct = (e) => {
		e.preventDefault()

		fetch(`https://intense-castle-41238.herokuapp.com/api/products/add`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Success",
					icon: "Success",
					text: "Product successfully added"
				})

				
				closeAdd()

				setName("")
				setDescription("")
				setPrice(0)
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})

					
			}
		})
	}

	
	return (
	<Fragment>
		<Row className="mt-3 mb-3 ">
			<Col xs={12} md={6}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>Retrieve All Products</Card.Title>
						<Card.Text>
							Get a list of all products in the database.
						</Card.Text>
						<Button variant="primary" as={Link} to={`/allproducts`}>Retrieve</Button>
					</Card.Body>
				</Card>
			</Col>
			
		<Col xs={12} md={6}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>Add New Product</Card.Title>
					<Card.Text>
						Add another exciting laptop to our database.
					</Card.Text>
					<Button variant="primary" onClick={openAdd}>Add New Product</Button>
				</Card.Body>
			</Card>
		</Col>
	</Row>
		{/*Add Course Modal*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>	

	

























	</Fragment>
	)
}