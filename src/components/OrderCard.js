
import {Card} from'react-bootstrap';


export default function OrderCard({orderProp}){
	//Use the state hook for this component to be able to store its state
	/*
		Syntax:
			const [getter,setter] = useState(initialGetterValue)
	
	*/

	const {productId,quantity,paymentType,checkOutDate}=orderProp;
	return (
			<Card className="prodCard">
				<Card.Body>
					
					<Card.Subtitle>Product Id:</Card.Subtitle>
					<Card.Text>{productId}</Card.Text>
					<Card.Subtitle>Quantity:</Card.Subtitle>
					<Card.Text>{quantity}</Card.Text>
					<Card.Subtitle>Payment Type:</Card.Subtitle>
					<Card.Text>{paymentType}</Card.Text>
					<Card.Subtitle>Check Out Date:</Card.Subtitle>
					<Card.Text>{checkOutDate}</Card.Text>
				</Card.Body>	
				
			</Card>

		)
}