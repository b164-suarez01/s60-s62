
import {Card,Button} from'react-bootstrap';
import {Link} from 'react-router-dom'

export default function ProductCard({productProp}){
	//Use the state hook for this component to be able to store its state
	/*
		Syntax:
			const [getter,setter] = useState(initialGetterValue)
	
	*/

	const {name,description,price,_id}=productProp;
	return (
			<Card className="prodCard">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
				
					<Button variant="primary" as={Link} to={`/products/${_id}`}>Details</Button>
				</Card.Body>	
				
			</Card>

		)
}