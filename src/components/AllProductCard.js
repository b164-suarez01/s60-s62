import {Card} from'react-bootstrap';


export default function AllProductCard({allproductProp}){
	//Use the state hook for this component to be able to store its state
	/*
		Syntax:
			const [getter,setter] = useState(initialGetterValue)
	
	*/

	const {description,price,_id,}=allproductProp;
	return (
			<Card className="prodCard">
				<Card.Body>
					<Card.Subtitle>Product Id:</Card.Subtitle>
					<Card.Text>{_id}</Card.Text>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
								
				</Card.Body>	
				
			</Card>

		)
}