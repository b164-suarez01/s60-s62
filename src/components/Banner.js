import {Row,Col,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'



export default function Banner(){
	return(

		<Row className="banner">
			<Col xs={12} md={3} className="headline">
				<h1 className = "brand">The Laptop Shop</h1>
				<p className="subtitle">Your reliable and trusted source of affordable laptops </p>
				<Button variant="primary" as={Link} to={`/products`}>Shop Now!</Button>
			</Col>
			<Col xs={12} md={9}>
				<div className="main-container">
					<img src = "./images/laptop.jpg" className="bgimage" alt="laptop picture"/>
				</div>
			</Col>
			
		</Row>		
		)		
}
