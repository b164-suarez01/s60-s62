import React from 'react';

export default function FooterPage() {
  return (
    <div className="footerDiv">
      <p>The Laptop Shop by Benjamin Suarez 2022 | Copyright Notice: For Educational Purposes Only</p>
    </div>
  );
};

