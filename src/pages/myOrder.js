import {Fragment,useEffect, useState} from 'react';
import OrderCard from '../components/OrderCard';
//import coursesData from '../data/coursesData';




export default function UserOrders(){
	const token = localStorage.getItem("token");
	console.log(token);

	const [products,setProducts] = useState([])
	//console.log(coursesData[0])
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp={course} />
	// 		)
	// })
	//Lalabas lahat ng active courses
	useEffect(() => {
		fetch('https://intense-castle-41238.herokuapp.com/api/users/userDbase')
		.then(res => res.json())
		.then(data => {
			
			
			setProducts(data.map(user => {
				return(
					
					//(user._id == "6261327b097ab8709d160db1") ?

					<OrderCard key={user._id} orderProp={user} />
				
					)

			}))
			
	
		})
	}, [])
	return (
		<Fragment>
			<h1 className="startLine">My Orders (Page Under Development!)</h1>
			 {products}
		</Fragment>
		)
}
