import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

export default function Login () {

	const {user,setUser} = useContext(UserContext);

	//These are the state hooks used to store the values of the input fields
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	
	
	//State to determine whether the button is enabled or not
	const [isActive, setIsActive] = useState(false);


	function authenticate(e){
		//prevents page redirection via form submission
		e.preventDefault()
		//Clear Input Fields
		/*
		Syntax:
			fetch("URL",{options})
			.then(res => res.json)
			.then(data => {})
		*/
		fetch('https://intense-castle-41238.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				//eto ung type type ng data na isesend sa backend
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			if(typeof data.accessToken !== "undefined"){
				
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)


		Swal.fire({

				title:"Login Successful",
				icon: "Success",
				
		});

			} else {
				Swal.fire({

						title:"Authentication failed",
						icon: "error",
						text: "Check your login details and try again!"
			})
			}
		})

		setEmail("");
		setPassword("");
	
	/*
		Syntax:
			localStorage.setItem("propertyName",value)
			store the email inside the localStorage
	*/
		//localStorage.setItem("email",email);

		//Set the global user state to have properties from local storage
		// setUser({
		// 	//getItem() getting the email inside the localStorage
		// 	email: localStorage.getItem('email')
		// })

		//alert(`${email} has been verified. Welcome back`);
	}


	//retrieve user detail na fxn
	const retrieveUserDetails = (token) => {
		fetch('https://intense-castle-41238.herokuapp.com/api/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
				

		//ung global user state, ung laman niya magrere-assign tayo dito
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
			
		})
	}


	//This is for form validation to enable the submit button when all input fields are populated and both passwords match
	useEffect(()=> {
		if((email !== "" && password !== "")) {
			setIsActive(true)}
		else {
			setIsActive(false)
		}
		},[email,password]
	)

	return( 
		//the page will automatically redirect the user to the products upon accessing the login route
	

	(user.id !== null && user.isAdmin === false) ?
		<Navigate to="/products" /> 
		:

	(user.id !== null && user.isAdmin === true) ?
		<Navigate to="/adminPage" /> 
		:

		<Form onSubmit ={(e)=> authenticate(e)}>
		<h1 className="text-center">Login</h1>
		  <Form.Group 
		  		className="mb-3" 
		  		controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    	type="email"
		    	placeholder="Enter email" 
		    	value={email}
		    	onChange = {e => {
		    		setEmail(e.target.value)
		         // console.log(e.target.value)}}
		     }}
		    	required
		    />

		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password"
		    	value={password}
		    	onChange={e => setPassword(e.target.value)}
		    	required />
		  </Form.Group>
			
		 {/*Conditionally render submit button based on the isActive*/
			isActive ?
			<Button variant="primary" type="submit" id="submitBtn">
			  Submit
			</Button>
			:
			<Button variant="danger" type="submit" id="submitBtn" disabled>
			  Submit
			</Button>
		  }
		</Form>
		
		)
}


















/*





	(user.id !== null && data.isAdmin==true) ?

		<Navigate to="/adminPanel" />
		:
		{

		 (user.id !== null && data.isAdmin!==true) ?

		<Navigate to="/products" />	

		:
		
		<Form onSubmit ={(e)=> authenticate(e)}>
		<h1 className="text-center">Login</h1>
		  <Form.Group 
		  		className="mb-3" 
		  		controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    	type="email"
		    	placeholder="Enter email" 
		    	value={email}
		    	onChange = {e => {
		    		setEmail(e.target.value)
		         // console.log(e.target.value)}}
		     }}
		    	required
		    />

		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password"
		    	value={password}
		    	onChange={e => setPassword(e.target.value)}
		    	required />
		  </Form.Group>
		 
		*/
		 /*Conditionally render submit button based on the isActive*/
	/*		isActive ?
			<Button variant="primary" type="submit" id="submitBtn">
			  Submit
			</Button>
			:
			<Button variant="danger" type="submit" id="submitBtn" disabled>
			  Submit
			</Button>
		 }
		</Form>
		}
*/







