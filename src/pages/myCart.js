import { Table } from 'react-bootstrap';
import {Fragment,useEffect, useState} from 'react';


export default function getCartItems(){

const [cartProducts,setCartProducts] = useState([])
	//console.log(coursesData[0])
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp={course} />
	// 		)
	// })
	//Lalabas lahat ng active courses
	useEffect(() => {
		fetch('https://intense-castle-41238.herokuapp.com/api/users/cartItems')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			//need i-map kasi ung data ay array of courses
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product} />
					)
			}))
		})
	}, [])
	return (
		<Fragment>
			<h1>Product</h1>
			 {products}
		</Fragment>
		)
}