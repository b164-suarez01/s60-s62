import { useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	//The useParams hook allows us to retrieve the courseId passed via the URL
	const { productId } = useParams()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);


	const addToCart = (productId) => {

		fetch("https://intense-castle-41238.herokuapp.com/api/users/addToCart", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data) 

			if (data === true) {

				Swal.fire({
					title:"You have successfully checked out this product!",
					icon: "Success",
					text: "Thank you for your purchase."
				})

				navigate("/products")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}


		})
	}

	useEffect(() => {
			console.log(productId)
			fetch(`https://intense-castle-41238.herokuapp.com/api/products/${productId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setName(data.name)
				setDescription(data.description)
				setPrice(data.price)
			})


		}, [productId])

	return(
			<Container className="mt-5">
				<Row>
					<Col lg={{span:6, offset:3}}>
						<Card>
					
							<Card.Body className="text-center" >
								<Card.Title>{name}</Card.Title>

								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>{price}</Card.Text>
								
								{
									user.id !== null ?
										<Button variant="primary" onClick={() => addToCart(productId)}>Check Outt</Button>
										:
										<Link className="btn btn-danger" to="/login">Log In to Purchase</Link>

								}
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		)

}