import {Fragment,useEffect, useState} from 'react';
import AllProductCard from '../components/AllProductCard';
//import coursesData from '../data/coursesData';


export default function AllProducts(){


	const [allproducts,setallProducts] = useState([])
	//console.log(coursesData[0])
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp={course} />
	// 		)
	// })
	//Lalabas lahat ng active courses
	useEffect(() => {
		fetch('https://intense-castle-41238.herokuapp.com/api/products/all')
		.then(res => res.json())
		.then(data => {
			
			//need i-map kasi ung data ay array of courses
			setallProducts(data.map(allproduct => {
				return(
					<AllProductCard key={allproduct._id} allproductProp={allproduct} />
					)
			}))
		})
	}, [])
	return (
		<Fragment>
			<h1 className="startLine">All Products</h1>
			 {allproducts}
		</Fragment>
		)
}