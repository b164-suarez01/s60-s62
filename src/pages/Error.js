//This page will display an error message if a client would attempt to go to a route/path that is not recognized or registered by the app.

import {Row,Col,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

//Create a function that will display an error message page
//The message will tell that the function /service/page is Not found. We need to use the proper HTTP status code to inform the user better
export default function ErrorPage(){
	return(
		// <Container>
		// 	<h1 className="text-center text-danger">404 Page Not Found</h1>
		// </Container>
		<Row>
			<Col className="p-5">
				<h1>404 - Not Found</h1>
				<p>The page you are looking cannot be found</p>
				<Button variant="primary" as={Link} to="/">Back to Home</Button>
			</Col>
		</Row>	

		);
};