import {Fragment,useEffect, useState} from 'react';
import OrderCard from '../components/OrderCard';


// import { useState, useEffect, useContext} from 'react';
// import {Container, Row, Col, Card, Button} from 'react-bootstrap';
// import {useParams, useNavigate, Link} from 'react-router-dom';
// import Swal from 'sweetalert2';
// import UserContext from '../UserContext';


export default function OrderHistoryView() {


	// const { user } = useContext(UserContext);
	// const navigate = useNavigate();

	// //The useParams hook allows us to retrieve the courseId passed via the URL
	// const { userId } = useParams()

	// const [productId, setProductId] = useState("");
	// const [quantity, setQuantity] = useState("");
	// const [paymentType, setPaymentType] = useState("");
	// const [checkOutDate, setcheckOutDate] = useState("");



	const [userOrders,setUserOrders] = useState([]);


	useEffect(() => {
			
			fetch("https://intense-castle-41238.herokuapp.com/api/users/userOrder", {
		
			headers: {
				
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		
		})
			.then(res => res.json())
			.then(data => {
            
			setUserOrders(data.map(product => {
				return(
					<OrderCard key={product._id} orderProp={product} />
					)
			}))
		})
	}, [])
	return (
		<Fragment>
			<h1 className="startLine">Order History</h1>
			 {userOrders}
		</Fragment>
		)
}




















// 				console.log(data)

// 				setName(data.name)
// 				setDescription(data.description)
// 				setPrice(data.price)
// 			})


// 		}, [productId])

// 	return(
// 			<Container className="mt-5">
// 				<Row>
// 					<Col lg={{span:6, offset:3}}>
// 						<Card>
					
// 							<Card.Body className="text-center" >
// 								<Card.Title>{productId}</Card.Title>

// 								<Card.Subtitle>Description:</Card.Subtitle>
// 								<Card.Text>{quantity}</Card.Text>
// 								<Card.Subtitle>Payment Type:</Card.Subtitle>
// 								<Card.Text>{paymentType}</Card.Text>
						
								
// 							</Card.Body>
// 						</Card>
// 					</Col>
// 				</Row>
// 			</Container>
// 		)

// }