import {Fragment,useEffect, useState} from 'react';
import ProductCard from '../components/ProductCard';



export default function Products(){
	
	const [products,setProducts] = useState([])
	
	//console.log(coursesData[0])
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp={course} />
	// 		)
	// })
	//Lalabas lahat ng active courses
	useEffect(() => {
		fetch('https://intense-castle-41238.herokuapp.com/api/products/')
		.then(res => res.json())
		.then(data => {
			
			//need i-map kasi ung data ay array of courses
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product} />
					)
			}))
		})
	}, [])

	return (
		<Fragment>
			<h1 className="startLine">Active Products</h1>
			 {products}
		</Fragment>
		)
}